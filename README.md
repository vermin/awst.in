# [awst.in](https://awst.in)

Playground for various web technologies.

Over time, this site has been built with Dart, CoffeeScript, Jekyll, Next.JS, now using [Gatsby](https://github.com/kripod/gatsby-starter-modern)

## 🚀 Getting started

- Clone this project: `git clone https://gitlab.com/vermin/awst.in`
- Install Docker and Docker-Compose (if necessary)
  - This project uses Docker to isolate the development environment from the local system, making the project portable and self-contained.
- Run the project with `docker-compose up`
- Tweak the docker_entrypoint.sh file if you want to execute a script `yarn develop` (see package.json) or arbitrary command

### Working with Docker

Running the container

`docker-compose up`

Building the base image. Useful when updating fundamentals like Node or Yarn

`docker build -t awst.in:latest -t awst.in:VERSION .` 

Connecting to a running container 

`docker exec -it CONTAINER_NAME COMMAND` where command is typically `/bin/bash`

See which containers are running

`docker container ls -a`

Remove ALL containers

`docker rm $(docker ps -aq)`       

Purge ALL images

`docker rmi $(docker image ls -aq)` 

### Deploy

The build somewhat depends on what you're targeting, but in general if you need to do a manual one (e.g. not using CI) modify `docker_entrypoint.sh` so that it runs `yarn gatsby build`. The `public` directory is a deployable static site at this point. 

[script-lint]: #linting
[script-format]: #automatic-code-formatting

If you're using GitLab, see the .gitlab-ci.yml file for CI/CD settings

## 💅 Style management

Originally using [Rebass][], a React UI component library & design system, is used for styling components. Based on [styled-components][] and [styled-system][], it provides a delightful way for managing styles.

[rebass]: https://jxnblk.com/rebass/
[styled-components]: https://www.styled-components.com/
[styled-system]: https://jxnblk.com/styled-system/

considering moving to [Emotion][] as there are some interesting performance claims without significant API differences

[emotion]: https://emotion.sh/  

## ✨ Developer experience

### Automatic code formatting

[Prettier][] is an opinionated code formatter aiming to provide codebase consistency when multiple developers work on the same project. The main reason behind adopting Prettier is to [stop all the on-going debates over coding styles][].

[prettier]: https://prettier.io/
[stop all the on-going debates over coding styles]: https://prettier.io/docs/en/why-prettier.html

### Linting

[Linters][lint] are tools that analyze source code to flag programming errors, bugs, stylistic errors, and suspicious constructs.

- JavaScript is linted by [ESLint][], enforcing the [Airbnb JavaScript Style Guide][] through an overridable set of rules provided by [eslint-config-airbnb-base][].
- Styles are linted by [stylelint][], adhering to the rules specified in [stylelint-config-recommended][].

[lint]: https://en.wikipedia.org/wiki/Lint_(software)
[eslint]: https://eslint.org/
[airbnb javascript style guide]: https://github.com/airbnb/javascript
[eslint-config-airbnb-base]: https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base
[stylelint]: https://stylelint.io/
[stylelint-config-recommended]: https://github.com/stylelint/stylelint-config-recommended
