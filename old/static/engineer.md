# Basics

- I've cut PSDs into tidy responsive accessible progressively enhanced mobile friendly websites.
- I've stood up production stacks in Linux and Windows, and on a handful of different cloud hosts.
- I've been hacked and had to climb back, so I know what hurts and how to avoid it.
- I've designed and built a bunch of different kinds of applications, APIs, and clients; primarily for Ecommerce, AdTech, and B2C SaaS.
- I've recruited engineers.
- I've collected customer requirements and handled partner relationships.
- I've wireframed and animated UIs for others to implement.
- I've run network installations for businesses and handled all aspects of IT.
- I enjoy tests, QA, and code reviews.
- I enjoy data viz.
- I'm getting better at data science.

# The Early Years (90s and 00s)

I've had a computer nearly as long as I can remember. I was lucky enough to have access to a mid-1990s ISP my uncle ran. We had ISDN back when 28.8 modems were the standard. The excitement of cutting the coax to wire up the house at age 10... 

"I've had a computer nearly as long as I can remember. I was lucky enough to have access to a mid-1990s ISP my uncle ran. We had ISDN back when 28.8 modems were the standard. The excitement of cutting the coax to wire up the house at age 10... 

My desire to learn how things tick has been with me since before the time I put my first PC together and installed SuSE linux (_1998_). By 2000 I was building websites and deploying computer networks. I am passionate about usability, maintainability, and security.
I read a lot. Usually books, blogs, tweets on topics where I'm looking to level up. I am largely self taught, although, in the 2000-2003 era, I attained
- CompTIA A+ certification
- Compaq/HP Printer Repair certification, sponsored by my employer at the time
- AP qualifying scores for college credit in Computer Science

Additionally, I studied for, but did not take the CompTIA Network+ exam.


# late 2000s (things get serious)

I started getting more excited about graphic design and implementations, which led me down the road of CSS
I worked as a repair tech during these years and learned the ropes. I couldn't tell you how many homes, NOCs, and offices I visited as I repaired PC and Mac harware, software, and network issues. Mainly, I learned to be a technician."


# 2010s

