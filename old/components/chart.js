/*
 * Docs on using react and d3
 * https://medium.com/@tibotiber/react-d3-js-balancing-performance-developer-experience-4da35f912484
 * https://github.com/airbnb/javascript/tree/master/react (general guidelines)
 * http://blockbuilder.org/search
 * look into canvas - https://plnkr.co/edit/fWtnsX?p=info
 */


import React from 'react'
import * as d3 from 'd3'
import _ from 'lodash'

class Chart extends React.Component {
  constructor(props) {
    super(props)
    // this.histogram = d3.layout.histogram();
    // this.widthScale = d3.scaleLinear().range([0, width])
    // this.yScale = d3.scaleLinear();
}

  componentDidMount() {
     //componenent must exist before calling d3 to avoid issues selecting DOM elems
     // console.log(d3.values(this.props.data))
     // console.log(Object.keys(this.props.data))
     // console.log(d3.entries(this.props.data))
     this.createChart()
  }

  componentDidUpdate() {
     this.createChart()
  }
  
  //EXAMPLE 2
  // https://www.dashingd3js.com/svg-text-element
  //Circle Data Set
  // var circleData = [
  //   { "cx": 20, "cy": 20, "radius": 20, "color" : "green" },
  //   { "cx": 70, "cy": 70, "radius": 20, "color" : "purple" }];

  //Create the SVG Viewport
  // var svgContainer = d3.select("body").append("svg")
  //                                      .attr("width",200)
  //                                      .attr("height",200);

  //Add circles to the svgContainer
  // var circles = svgContainer.selectAll("circle")
  //                            .data(circleData)
  //                            .enter()
  //                            .append("circle");

  //Add the circle attributes
  // var circleAttributes = circles
  //                        .attr("cx", function (d) { return d.cx; })
  //                        .attr("cy", function (d) { return d.cy; })
  //                        .attr("r", function (d) { return d.radius; })
  //                        .style("fill", function (d) { return d.color; });

  //Add the SVG Text Element to the svgContainer
  // var text = svgContainer.selectAll("text")
  //                         .data(circleData)
  //                         .enter()
  //                         .append("text");

  //Add SVG Text Element Attributes
  // var textLabels = text
  //                  .attr("x", function(d) { return d.cx; })
  //                  .attr("y", function(d) { return d.cy; })
  //                  .text( function (d) { return "( " + d.cx + ", " + d.cy +" )"; })
  //                  .attr("font-family", "sans-serif")
  //                  .attr("font-size", "20px")
  //                  .attr("fill", "red");

  //EXAMPLE 2
  // https://bl.ocks.org/mbostock/7341714
  // var width = 420,
  //     barHeight = 20;

  // var x = d3.scale.linear()
  //     .range([0, width]);

  // var chart = d3.select(".chart")
  //     .attr("width", width);

  // d3.tsv("data.tsv", type, function(error, data) {
  //   x.domain([0, d3.max(data, function(d) { return d.value; })]);

  //   chart.attr("height", barHeight * data.length);

  //   var bar = chart.selectAll("g")
  //       .data(data)
  //     .enter().append("g")
  //       .attr("transform", function(d, i) { return "translate(0," + i * barHeight + ")"; });

  //   bar.append("rect")
  //       .attr("width", function(d) { return x(d.value); })
  //       .attr("height", barHeight - 1);

  //   bar.append("text")
  //       .attr("x", function(d) { return x(d.value) - 3; })
  //       .attr("y", barHeight / 2)
  //       .attr("dy", ".35em")
  //       .text(function(d) { return d.value; });
  // });

  // function type(d) {
  //   d.value = +d.value; // coerce to number
  //   return d;
  // }

  // EXAMPLE 3
  // var Bar = React.createClass({
  // getDefaultProps: function() {
  //   return {
  //     data: []
  //   }
  // },
  
  // shouldComponentUpdate: function(nextProps) {
  //     return this.props.data !== nextProps.data;
  // },
  
  // render: function() {
  //   var props = this.props;
  //   var data = props.data.map(function(d) {
  //     return d.y;
  //   });
  
  //   var yScale = d3.scale.linear()
  //     .domain([0, d3.max(data)])
  //     .range([0, this.props.height]);
  
  //   var xScale = d3.scale.ordinal()
  //     .domain(d3.range(this.props.data.length))
  //     .rangeRoundBands([0, this.props.width], 0.05);
  
  //   var bars = data.map(function(point, i) {
  //     var height = yScale(point),
  //         y = props.height - height,
  //         width = xScale.rangeBand(),
  //         x = xScale(i);
  
  //     return (
  //       <Rect height={height} 
  //             width={width} 
  //             x={x} 
  //             y={y} 
  //             key={i} />
  //     )
  //   });
  
  //   return (
  //         <g>{bars}</g>
  //   );
  // }
  // });    

  createChart() {
    const width = this.props.size[0]
    const barHeight = 40

    var x = d3.scaleLinear().range([0, width])

    const svg = d3.select("svg")

    const bar = svg.selectAll("g")
      .data(d3.entries(this.props.data))
      .enter()
        .append("g")
        .attr("transform", (d, i) => "translate(0," + i * barHeight + ")" )

       
    bar
     // .scale(x)
     .append("rect")
     .attr("width", 1+"px") //x.bandwidth())
     // .attr("height", _.sum(d3.values(d3.values(this.props.data)))
     .attr("height", 19)
     .attr("class", "bar")
     .style("stroke", "black")
     .style("stroke-width", "1")
     .style("stroke-dasharray", "5,3,2")
     .style("fill", "#999")
     .style("fill-opacity", ".50")
     .transition().duration(2000)
     .attr("width", (d) => (d.value * 40)+"px") //x.bandwidth())
         // .attr("x", 0)
         // .attr("y", (d) => d * 20)
         // .attr("y", (d, i) => i*10)   //function(d) { return y(d.frequency); })
         // .attr("x", (d, i) => d*10)   //function(d) { return y(d.frequency); })
         // .attr("width", (d) => this.props.data[d]) //x.bandwidth())
         // .attr("width", (d) => d3.values(this.props.data)* 10 ) //x.bandwidth())

    bar
    // .selectAll("Link")
    .append( "a" )
    .attr("href", ( d => "/" + d.key.toLowerCase())) 

    // bar
      // .select("a")
      .append("text")
      // .select("text")
      .attr('opacity', 0)
      .attr("x", 3)
      .attr("y",14)
      .attr("text-anchor", "start")
      .style("font-family", "Arial")
      .style("stroke", "#222")
      .classed("portal", true)
      .text( d => d.key )
      .transition().duration(10000)
      .attr('opacity', 100)

  }
   
   render() {
       // https://medium.com/@Elijah_Meeks/interactive-applications-with-react-d3-f76f7b3ebc71
       return <svg width={this.props.size[0]} height={this.props.size[1]}>
       </svg>

       // via d3+react guide
       // const translate = "translate(0, "+this.props.topMargin+")";
       // return <g className="histogram" transform={translate}></g>
   }

}
export default Chart

