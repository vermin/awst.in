import _ from 'lodash'

import Logo from '../components/logo'

class SkillSection extends React.Component {

    constructor(props) {
      super(props)
    }

    static async getInitialProps() {
    }

    render() {
      // _.forIn(this.props.items, (m) => {_.map(m, (v,i) => console.log(v))})
      console.log(this.props.items)
      return( 
        <div>
          <h3>{this.props.title}</h3>
          {_.map(this.props.items, (item) => { 
              return <Logo logo={item.logo} title={item.title} site={item.site} />
            }
          )}
        </div>
      )
    }
}

export default SkillSection
