class Logo extends React.Component {

    constructor(props) {
        super(props)
    }

    static async getInitialProps() {}

    render() {
      return( 
        <figure>
          <a href={ this.props.site } target="_blank">
            <img src={ 'static/img/'+ this.props.logo } alt={ this.props.title } width="300px" height="300px"/>
            <figcaption> {this.props.title} </figcaption>
          </a>
        </figure>
        )
    }
}

export default Logo
