import React from 'react'
import * as d3 from 'd3'
import 'isomorphic-fetch'
import ReactMarkdown from 'react-markdown'
import _ from 'lodash'

import Header from '../components/header'
import Chart from  '../components/chart'
import SkillSection from '../components/skillSection'

import front from  '../static/engineer'

// eslint-disable-next-line no-undef
class Engineer extends React.Component {

    static async getInitialProps() { 
        const content = markdown.require('../static/engineer.md')
        // const content = await fetch('http://localhost:3000'+ front.content)
        // const body = await content.toString()
        return { front : front, content : content }
    }

    componentWillMount() {   }

    componentDidMount()  {   }

    render() {
      // console.log("groups")
      // console.log(groups)
      // console.log("items")
      // console.log(items)

      const sections = _.keys(this.props.front.tree).map( (heading, key) => 
        <SkillSection title={heading} items={this.props.front.tree[heading]} />
      )
        
    return (
        
    <div>
        <Header>
        </Header>
        <aside>{this.props.front.quote}</aside>
        <article> 
            <p>hello, I engineer stuff </p>
            { sections }
            
            <ReactMarkdown source ={ this.props.content } />
            {/* <Chart data={this.props.front.tree} size={[700,400]}/> */}
        </article>
    </div>
    )}
}


export default Engineer;
