import React from 'react';
import * as d3 from 'd3';

import Header from '../components/header'
import human from '../static/human'
import Chart from '../components/chart'

// eslint-disable-next-line no-undef
class Human extends React.Component {

    static async getInitialProps() {
        console.log(Object.keys(human))
        const h = Object.keys(human.generalskills)
        return { categories: h, human: human }
    }

    componentDidMount() {    }

    render() {
    return (
        
    <div>
        <Header>
        </Header>
        <article> 
            <p>hello, I'm a person</p>
            <Chart data={this.props.human.generalskills} size={[700,400]}/>
        </article>
    </div>
    )}
}


export default Human;
