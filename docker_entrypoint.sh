#!/bin/sh
set +e

cd /app

yarn install

# yarn write-good
# node --inspect-brk --no-lazy node_modules/gatsby/dist/bin/gatsby develop
yarn outdated
yarn format
yarn lint --fix
# yarn eslint src/**/*.{js,jsx}
# /app/node_modules/.bin/eslint --ext .js,.jsx --ignore-pattern public --ignore-pattern old --fix src/**/*.{js,jsx,json,md}
# yarn eslint --ext .js,.jsx --ignore-pattern public --ignore-pattern old --fix src/**/*.{js,jsx,json,md}
# yarn clean
# yarn build
# yarn gatsby serve -H 0.0.0.0 -p 4455 #host flag doesn't seem to work :(
yarn gatsby develop -H 0.0.0.0 --verbose
