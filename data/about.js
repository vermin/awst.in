export const what = [
  {
    title: "Let me help you with:",
    entries: [
      {
        name: "Product Development and Management",
        link: "pm"
      },
      {
        name: "Web Application Development",
      },
      {
        name: "User Experience and Accessibility",
      },
      {
        name: "Information Architecture",
      },
      {
        name: "Data Science and Analysis",
      },
      {
        name: "Hiring and Team Development",
      },
      {
        name: "Developer Operations (DevOps)",
      },
      {
        name: "Technical Writing",
      },
      {
        name: "Policy Analysis",
      },
      {
        name: "Audio / Visual Pre/Post Production and Editing"
      },
    ]
  },
  {
    title: "Industry Experience",
    entries: [
      {
        name: "Programmatic Digital Advertising",
      },
      {
        name: "Ecommerce and Retail",
      },
      {
        name: "Print Advertising",
      },
      {
        name: "Telecommunications and Networking",
      },
      {
        name: "Political Campaign Management",
      },
      {
        name: "Film Production and Distribution ",
      },
      {
        name: "Music Production and Management"
      },
    ]
  },
  {
    title: "Tech I'm intested In:",
    entries: [
      {
        name: "Web Standards",
      },
      {
        name: "Distributed (Peer to Peer) Web",
      },
      {
        name: "Realtime Communication",
      },
      {
        name: "Information Security",
      },
      {
        name: "Data Visualization",
      },
    ]
  },
  {
    title: "Some Tools I think are cool",
    entries: [
      {
        name: "Beaker Browser"
      },
      {
        name: "Lighthouse"
      }
    ]
  },
  {
    title: "Speaking / Presentations",
    entries: [
      {
        name: "reveal.js",
      },
      {
        name: "PowerPoint"
      },
    ]
  },
  {
    title: "Business Software",
    entries: [
      {
        name: "Atlassian Suite",
      },
      {
        name: "Google Suite",
      },
      {
        name: "Excel",
      },
      {
        name: "SalesForce",
      },
      {
        name: "Zoho",
      },
      {
        name: "NetSuite"
      },
    ]
  },
  {
    title: "Browser-land",
    entries: [
      {
        name: "Javascript IE4 - modern (ES6/Babel)",
      },
      {
        name: "Chrome Inspector",
      },
      {
        name: "Sass",
      },
      {
        name: "Coffeescript",
      },
      {
        name: "Ember",
      },
      {
        name: "React",
      },
      {
        name: "jQuery",
      },
      {
        name: "Modernizr",
      },
      {
        name: "Gulp"
      },
    ]
  },
  {
    title: "Server/Cloud",
    entries: [
      {
        name: "Docker",
      },
      {
        name: "Vagrant",
      },
      {
        name: "*nix: Debian, Ubuntu, FreeBSD, RHEL",
      },
      {
        name: "Nginx",
      },
      {
        name: "Apache",
      },
      {
        name: "AWS",
      },
      {
        name: "Heroku",
      },
      {
        name: "Certbot / Let's Encrypt",
      },
      {
        name: "Cloudflare",
      },
      {
        name: "OpenStack",
      },
      {
        name: "Rackspace"
      },
    ]
  },
  {
    title: "Web Frameworks / CMS / Ecommerce",
    entries: [
      {
        name: "Ruby-on-Rails",
      },
      {
        name: "WordPress",
      },
      {
        name: "Gatsby",
      },
      {
        name: "SpreeCommerce",
      },
      {
        name: "Shopify"
      },
    ]
  },
  {
    title: "General Programming",
    entries: [
      {
        name: "Ruby",
      },
      {
        name: "Python",
      },
      {
        name: "Jupyter",
      },
      {
        name: "Anaconda ",
      },
      {
        name: "PHP",
      },
      {
        name: "C#",
      },
      {
        name: "Java",
      },
      {
        name: "A tiny bit of Rust and Go"
      },
    ]
  },
  {
    title: "Data Science / DataViz",
    entries: [
      {
        name: "Pandas",
      },
      {
        name: "Numpy",
      },
      {
        name: "D3",
      },
      {
        name: "Bokeh"
      },
    ]
  },
  {
    title: "Other",
    entries: [
      {
        name: "Git",
      },
      {
        name: "P4",
      },
      {
        name: "Subversion",
      },
    ]
  },
  {
    title: "Testing",
    entries: [
      {
        name: "Nose",
      },
      {
        name: "Rspec",
      },
      {
        name: "Minitest",
      },
      {
        name: "Jest",
      },
    ]
  }
]

export const location = [{
      content: "I am located in Austin, Texas.\r\nI have degrees in Communications, Legal Institutions, Economics, Political Science, and Audio Engineering."
    }]
