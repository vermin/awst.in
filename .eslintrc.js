module.exports = {
  "env": {
    "browser": true,
    "es6": true,
  },
  "extends": ["airbnb","prettier"],
  "plugins": [
    "react",
  ],
  "globals": {
    "graphql": false,
  },
  "parserOptions": {
    "ecmaVersion": 9,
    "ecmaFeatures": {
      "jsx": true,
    },
    "sourceType": "module",
  }
}
