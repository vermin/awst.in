# Useful links

## JSX and D3

- http://www.adeveloperdiary.com/react-js/integrate-react-and-d3/
  - All D3 code in render() [ed: design choice? maybe encapsulation or private-like method?]; ES5

- https://medium.com/@sxywu/on-d3-react-and-a-little-bit-of-flux-88a226f328f3
  - really nice compare / contrast btwn d3 and react+d3 implementations of components. +transitions

- https://hackernoon.com/how-and-why-to-use-d3-with-react-d239eb1ea274

- https://github.com/joelburget/d4

- https://mikewilliamson.wordpress.com/2016/06/03/d3-and-react-3-ways/

- https://blog.sicara.com/a-starting-point-on-using-d3-with-react-869fdf3dfaf

- https://medium.com/@tibotiber/react-d3-js-balancing-performance-developer-experience-4da35f912484

- https://10consulting.com/2014/02/19/d3-plus-reactjs-for-charting/

- https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#what-about-memoization

### libs and utils

- https://github.com/d3/
  - https://github.com/d3/d3-ease
  - https://github.com/d3/d3-transition

- https://github.com/Olical/react-faux-dom/blob/master/DOCUMENTATION.md

- https://github.com/esbullington/react-d3/blob/master/src/linechart/LineChart.jsx

## Style Guides

- https://github.com/airbnb/javascript

- https://github.com/airbnb/javascript/tree/master/react

- https://bocoup.com/blog/the-many-faces-of-functions-in-javascript 
  - not exactly a guide but...

## D3 / Bl.ocks

- http://bl.ocks.org/sxywu/fcef0e6dac231ef2e54b
  - mainly uses d3 for DOM mgmt inside svg

- https://github.com/sxywu/expenses/blob/master/scripts/visualizations/Expense.js 

- https://github.com/tibotiber/rd3/blob/master/src/components/charts/ScatterPlot.js

- https://github.com/emeeks/d3_in_action_2/blob/master/chapter9/reactd3/src/BarChart.js
  - seems a little dated and maybe does some weird DOM ID passing?

- http://bl.ocks.org/sxywu/61a4bd0cfc373cf08884
  - anti-pattern
  - animation and react
  - Con:
    * Goes through lifecycle for every tick -> will not scale
    * Cannot use D3 functions that need access to DOM 

- https://github.com/adeveloperdiary/react-d3-charts/tree/gh-pages/01_Visitor_Dashboard
  - react d3
  - analytics style dash

- https://bl.ocks.org/mbostock/1256572
  - transitions

- http://bl.ocks.org/herrstucki/9205264
  - transitions with react and d3

- http://bl.ocks.org/robschmuecker/7880033
  - Drag and Drop, Zoomable, Panning, Collapsible Tree with auto-sizing.

- https://github.com/Swizec/h1b-software-salaries/blob/es6-webpack-version/src/components/Histogram/Histogram.jsx

- http://bl.ocks.org/d3noob/c37cb8e630aaef7df30d
  - tool tip with HTML link

- http://bl.ocks.org/nnattawat/8916402
  - histogram with animations

- https://bl.ocks.org/Bl3f/cdb5ad854b376765fa99
  - heatmap

## D3

- https://medium.com/@mbostock/introducing-d3-scale-61980c51545f

- https://square.github.io/intro-to-d3/data-binding/

- http://alignedleft.com/tutorials/d3/binding-data/

- http://chimera.labs.oreilly.com/books/1230000000345/ch06.html#_the_new_chart

- https://www.dashingd3js.com/d3-v4-tutorial 

- https://leanpub.com/d3-t-and-t-v4

## SVG Animations and Transitions

- https://www.w3.org/Graphics/SVG/IG/resources/svgprimer.html
- https://www.w3.org/TR/SVG/propidx.html
  - svg names are a little different from css

- https://www.smashingmagazine.com/2014/11/styling-and-animating-svgs-with-css/

- https://www.sitepoint.com/tips-accessible-svg/


## CSS
- https://medium.com/@tkh44/emotion-ad1c45c6d28b
- https://alligator.io/react/react-emotion/

### libs and utils

- http://snapsvg.io/

- https://www.styled-components.com/


## Next.js

- https://github.com/now-examples/next-news/tree/master/lib

- https://jsmantra.com/faster-leaner-and-better-next-js-ea40afb94907

- https://github.com/zeit/next.js


## Testing

- https://mochajs.org/


## Future?

- 3d and WebGL with https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene

## Product

- https://www.productmanagerhq.com/2018/03/product-manager-interview-create-a-product-roadmap/

## Gatsby

- http://portfolio-v3.surge.sh/

- https://lengstorf.com/keeping-score/


## Help

- https://gitter.im/

## PWA

- https://developers.google.com/web/progressive-web-apps/checklist
- https://codelabs.developers.google.com/codelabs/offline/#5
- https://developers.google.com/web/fundamentals/primers/service-workers/
- https://developers.google.com/web/ilt/pwa/caching-files-with-service-worker#cachefallback
- https://developer.mozilla.org/en-US/docs/Web/API/Cache
- https://snipcart.com/blog/pwa-example-ecommerce-gatsby
- https://github.com/snipcart/gatsby-pwa-demo/
- https://developers.google.com/web/tools/workbox/modules/workbox-build
- https://github.com/GoogleChromeLabs/sw-precache
