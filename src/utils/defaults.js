export const colors = {
  primary: '#0C5C5F',
  secondary: '#26A6AC',
  axis: 'green',
  background: 'white',
  text: 'black',
};

export const sizing = {
  width: 640,
  height: 480,
  margin: {
    top: 20,
    right: 20,
    bottom: 10,
    left: 70,
  },
};
