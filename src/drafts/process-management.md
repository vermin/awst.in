---
date: '2018-11-09'
updated: '2018-11-09'
title: Thoughts on Managing Software Development Process
slug: 'managing-the-software-development-process'
---
I think there's a sleight of hand going on when people talk about managing software development as "herding cats" or "bringing clarity forth from chaos". By its very nature, software development is always breaking new ground, meaning there is discovery, complexity, and unknown unknowns.

Plenty of people have written some excellent books on how to manage people, projects, teams, and software development methodologies. A some of my favorites are:

- Behind Closed Doors
- Agile and Lean Program Management
- The Five Dysfunctions of a Team: A Leadership Fable  
- The Lean Startup

These books answer a lot of questions, and much better than the typical video you'll find on YouTube. My impression is that YouTube or conference style presentations often contain someone who has "seen it all" and has been doing the same work as you at very prestigious organizations, and often for much longer than you. They give you advice that starts "If I could go back in time, knowing what I know now..." then they hit you with "Do More X", followed quickly by "Don't Do X". It's enraging and makes you wonder if their real secret to success is being very good at double-talk. What I like about these books is that they generally acknowledge the fact that you are in a unique situation. Your team composition, your product, your organization, the market, and the color of the sky outside all create a nexus of unknowable events, which no "Do X" philosophy can capture.

So what does work? And what is a waste of time?

First, I believe it is important to take stock of where you are by making sure everyone is pulling in the same direction. You and your collaborators are going after the same goal after all, right?

# What, Why, Who
1. What problem are you solving?
2. Why is this problem worth the investment?
3. Whose problem are you solving? Who is your audience?

If you can't get everyone to agree about the answers to these questions then you're not likely to hit the mark. Take a great team, give them great tools, have a process that makes people feel empowered and not stymied, but fail to get concensus about the answers to these questions and everyone will be running in their own direction.


# Is Agile a religion?

You would think so based on the way management talks about it. But like any religion, you find that people are willing to adopt the parts that require little from them in the way of work, and feel comfortable ignoring or writing off the parts that are uncomfortable or challenging.

Is there anything wrong with that?
Only if you expect that your preferred methodology is a panacea. As with most things in life, we typically find that we only get out what we put in.

- https://18f.gsa.gov/2015/10/15/best-practices-for-distributed-teams/
