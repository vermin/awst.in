---
path: '/product-management'
title: 'Thoughts on Product Management'
date: '2018-08-23'
tags: ['Product Management']
---

Product Management, or, the art of selling ideas and converting those ideas into something tangible.

# How Did I Start?

I've always enjoyed designing things. I am a doodler and a tinkerer. Early in my career as a web developer I started spinning up all kinds of new projects. My employer entrusted me with bringing a few of these to market.

I had a few home-grown products and a half-dozen more white label products. The process of making these products consistent with the business model we were pushing, as well as fit the company's market positioning, was a great learning opportunity. I came up with the aesthetic design of the apps. I wrote nearly all of the marketing collateral. I developed a knowledge-base for the technical support team.

The key part of all of this was making sure that the _important_ details were covered. If you can't figure out what's important, it can be easy to spend all of your time chasing stuff ya ain't gonna need. The easiest way to get three (or more) conflicting opinions about something is to allow focus to drift to the wrong level of detail. If you're trying to demo _how_ a widget works, it is best not to include insignificant information. For example, if the important decision is where to place your widget on a web page, including a high-fidelity colorized mockup inevitably leads to debate about whether you're using the right shade of purple. Focus on what matters!

# What Makes a Product Successful?

A strong market can turn a mediocre product that would enjoy mild success in an average market into a success. Reality will always assert itself. No matter how good the team, no matter how well-designed, well-crafted the product, without a market that wants it, the product won't sell. You need to find the product/market fit.

## Questions to Ask

"""
Product management is the function of serving as a proxy to a defined set of markets (or market segments), in order to be able to ensure appropriate product creation, and ongoing product health and quality for those markets throughout a product’s entire lifecycle, until end of life.

To me, it’s important just to get this out there and then start to gather some feedback from others and then tweak based on the collective. 80% is close enough to start honing in on the precise solution
"""

Product development lifecycle / finding product-market fit

- Who are we creating value for? (personas)
- What are their underserved need (judged in relation to alternatives)
- Value prop (differentiators)
- feature set
- user experience

1. Align on the high-level objectives for the organization
2. Identify relevant metrics
3. Determine which customer segment to focus on
4. Identify customer needs and brainstorm features to solve those needs
5. Prioritize your features
6. Create a timeline of features and highlight dependencies
7. Discuss how various factors and assumptions might change your roadmap

Create a project/program charter (2-4 sentences)
Define the primary customers (who)
Define the benefit (why)
Define the problems the product solves

# What Makes a Great Product?

Everything you build is going to have room for improvement. The important part of making something is keeping your focus on the objective you wish to achieve. As often as not, people see that you have a simple thing that works well, and they contact you with a request for a "small addition". This is usually the first step toward building a swiss-army knife. You will do a lot of things, and having all of the things together is usually a convient form-factor, but it rarely leads toward any single thing being best-in-class.

I think the Unix philosophy is instructive here.

- Focus on doing a single, specific thing really well
- Ensure interoperability with other systems

It is easy to lose the thread of what you want to do by overloading your product with auxilliary functionality that doesn't directly advance the core product offering.

# Process

## Methodologies

Brief descriptions

### LEAN

1. Eliminate Waste
2. Amplify Learning
3. Decide as late as possible
4. Deliver as fast as possible
5. Empower the team
6. Build integrity in
7. See the whole

### AGILE

1. Deliver early and often to satisfy the customer
2. Welcome changing requirements
3. Deliver working software frequently
4. Business people and developers must work together
5. Trust motivated people to do their jobs
6. Face-to-face conversation is the most efficient and effective method of conveying information
7. Working software is the primary measure of progress
8. Maintain a sustainable pace
9. Continuous attention to technical excellence and good design enhances agility
10. Simplicity - the art of maximizing the amount of work not done is essential
11. The best architectures, requirements, and designs emerge from self organized teams
12. Reflect and adjust at regular intervals.

## Evaluating effort/ Retrospective Questions

Goals

Was It A Good Goal? Was It Clear?

If We Did It Again, What Would We Do Differently

How Did It Succeed?

Would More Planning Have Made The Project Go Faster?

Recommendations Going Forward

How Long Would It Have Taken Knowing What You Know Now?

What Went Well?

# Risks and Challenges
