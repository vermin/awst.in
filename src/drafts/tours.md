---
date: '2018-11-19'
updated: '2018-11-19'
title: Seeing Austin
slug: 'seeing-austin'
---

# The DayTripper
Easily one of the best shows. Pretty reliable that every episode will showcase a family friendly activity or two, and BBQ.

# Brews
People like to drink here. 

## [Family Business Beer Company](https://familybusinessbeerco.com/)
Pretty nice drive out there. Near Hamilton Pool. Cousin Dave gives a great tour. There was a cajun food truck when I went in November 2018.

