---
date: '2018-11-09'
updated: '2018-11-09'
title: List of Books and Essays
slug: 'useful-books'
---

This is a list of books and essays on various topics that I suggest you check out.

Topics:

- Software Development
- Product and Process Development
- Data Science
- Social Sciences
- Fiction

# Software Development

## Publishers

[Pragmatic Bookshelf](https://pragprog.com/)

As far as I know, the OG publisher for DRM free content. Don't be held hostage by _those_ books with the cute animals on the cover! The seminal Pragmatic Programmer is a title that should be required readin for anyone working near software.

[No Starch Press](https://nostarch.com/)

They've been pumping out good content for a while. I'm not totally certain that all of their books should be sold as early in the early access process as they do... but I've been pleased with titles like Klabnik and Nichols's [Rust](https://nostarch.com/Rust) book.

[Manning Press](https://www.manning.com/)

Notable publisher. I've read fewer books from these folks, but in general I think they have fewer errors than publishers like Wrox.
