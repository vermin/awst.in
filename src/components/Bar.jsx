// via https://www.codementor.io/blackmind/creating-graphs-as-react-components-with-d3-js-5x0qujojy
import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import Transition from 'react-transition-group/Transition';
// import { Transition} from 'react-transition-group';
// import {  CSSTransition,   TransitionGroup, } from 'react-transition-group';

import * as defaults from '../utils/defaults';

class Bar extends React.Component {
  defaultState = {
    height: 0,
    opacity: 0,
  };

  state = this.defaultState;

  node = React.createRef();

  componentDidMount() {
    // console.log("mounted")
    let el = d3.select(this.node.current);

    el.style('fill-opacity', 0)
      .transition()
      .duration(800)
      .ease(d3.easeSin)
      .style('fill-opacity', '1')
      .on('end', () =>
        this.setState({
          height: this.props.height,
        }),
      );
  }

  componentDidUpdate() {
    // console.log("updated")
    // let el = d3.select(this.node.current);
    // el
    // .attr('height', this.state.height)
    // .transition()
    //   .duration(800)
    //   .ease(d3.easeBounceOut)
    //   .attr('height', this.props.height)
    // .on('end', () =>
    // this.setState({
    // height: this.props.height,
    // }),
    //   );
  }

  componentWillUnmount() {
    let el = d3.select(this.node.current);

    el.attr('height', this.props.height)
      .transition()
      .duration(1800)
      .ease(d3.easeBounceOut)
      .attr('height', 0)
      .style('fill-opacity', 0)
      .on('end', () => this.setState(this.defaultState));

    this.state = this.defaultState;
  }

  render() {
    const { className, label } = this.props;
    return (
      <g className={className}>
        <rect ref={this.node} {...this.props} />
        <text x={this.props.x} y={this.props.y}>
          {label}
        </text>
      </g>
    );
  }
}

Bar.propTypes = {
  colors: PropTypes.object,
  width: PropTypes.number,
  height: PropTypes.number,
  x: PropTypes.number,
  y: PropTypes.number,
  className: PropTypes.string,
};

Bar.defaultProps = {
  className: 'bar',
  width: 10,
  height: 100,
  x: d3.randomUniform(1, 100)(),
  y: 1,
};

export default Bar;
