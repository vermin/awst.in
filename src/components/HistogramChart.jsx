import * as d3 from 'd3';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// http://bl.ocks.org/nnattawat/8916402
// <Histogram data={this.props.data} />
export default class HistogramChart extends Component {
  componentDidMount() {
    this.createChart();
  }

  componentDidUpdate() {
    d3.select(this.node)
      .selectAll('g')
      .remove();
    this.createChart();
  }

  createChart() {
    // assign display and data handles
    const node = this.node;

    const { colors, data, sizing, titlePrefix, xLabel, yLabel } = this.props;

    // set display bounds
    const chartWidth = sizing.width - sizing.margin.left - sizing.margin.right;
    const chartHeight =
      sizing.height - sizing.margin.top - sizing.margin.bottom;

    const x = d3
      .scaleLinear()
      .domain(d3.extent(data))
      .nice()
      .range([sizing.margin.left, chartWidth]);

    // chop up data for y axis
    // bins = [Array(6), Array(1), Array(2), Array(1), Array(1), Array(4), Array(0), Array(1)]
    //        0: (6) [1, 1, 1, 1, 1, 1, x0: 1, x1: 2]
    const histogram = d3.histogram().thresholds(x.ticks(9));

    const bins = histogram(data);

    const y = d3
      .scaleLinear()
      .domain(d3.extent(bins, d => d.length))
      .nice()
      .range([chartHeight, sizing.margin.top]);

    // const colorScale = d3.scale.threshold()
    //     .domain([0.85, 1])
    //     .range(["#2980B9", "#E67E22", "#27AE60", "#27AE60"]);

    const svg = d3
      .select(node)
      .style('background-color', colors.background)
      .style('width', sizing.width)
      .style('height', sizing.height)
      .attr('width', sizing.width)
      .attr('height', sizing.height);

    const xAxis = g =>
      g
        .attr('transform', `translate(0,${chartHeight})`)
        .call(d3.axisBottom(x).tickSizeOuter(0))
        .call(axisLabel =>
          axisLabel
            .append('text')
            .attr('x', sizing.width - sizing.margin.right)
            .attr('y', -4)
            .attr('fill', '#000')
            .attr('font-weight', 'bold')
            .attr('text-anchor', 'end')
            .text(xLabel),
        );

    const yAxis = g =>
      g
        .attr('transform', `translate(${sizing.margin.left},0)`)
        .call(d3.axisLeft(y))
        .call(axisLabel =>
          axisLabel
            .select('.tick:last-of-type text')
            .clone()
            .attr('x', 4)
            .attr('text-anchor', 'start')
            .attr('font-weight', 'bold')
            .text(yLabel),
        );

    svg
      .append('g')
      .style('fill', colors.primary)
      .selectAll('rect')
      .data(bins)
      .enter()
      .append('rect')
      .attr('x', d => x(d.x0) + 1) // increment horizontal position; basically an index
      .attr('width', d => Math.max(0, x(d.x1) - x(d.x0) - 1))
      .attr('y', d => y(d.length))
      // for when you dont have a 0 value you need the smallest value
      // in the dimension, i.e.
      // .attr("height", d => y(d3.min(yData)) - y(d.y))
      .attr('height', d => y(0) - y(d.length))
      .append('title')
      .text(d => `${titlePrefix + d.length}:${d.x0} - ${d.x1}`);
    // .attr('fill', function(d) { return colorScale(d.value); });

    svg.append('g').call(xAxis);

    svg.append('g').call(yAxis);

    svg
      .selectAll('g')
      .selectAll('rect')
      .data(bins)
      .exit()
      .remove();
  }

  render() {
    // Had been using the code below to get a reference
    return (
      <svg ref={node => (this.node = node)} className={this.props.className} />
    );
    // but styled components suggested this instead
    // return <StyledSVG ref={this.setInputRef}></StyledSVG>
  }
}

HistogramChart.propTypes = {
  data: PropTypes.array.isRequired, // [1, 1, 3, 4, 6, 8, ...],
  className: PropTypes.string,
  colors: PropTypes.object,
  sizing: PropTypes.object,
  titlePrefix: PropTypes.string,
  xLabel: PropTypes.string,
  yLabel: PropTypes.string,
  // data:           PropTypes.array.isRequired, //consider: chart = { data: [{x:3, y:4, label: "thundercloud"},], xlabel: "", ylabel: "" }
  // xRange: d3.time.scale().range([margin,width-margin]).domain([new Date(2011, 08, 15), new Date(2015, 01, 01)]);,
  // yRange: d3.scale.linear().range([height-margin, margin]).domain([77, 100]);,
  // r: d3.scale.linear().domain([50,200]).range([0,20]);
};

HistogramChart.defaultProps = {
  className: 'chart',
  sizing: {
    width: 441,
    height: 300,
    margin: {
      top: 20,
      right: 20,
      bottom: 50,
      left: 70,
    },
    barWidth: 10,
  },
  colors: {
    axis: 'green',
    background: 'white',
    primary: 'turquoise',
    text: 'green',
  },
  titlePrefix: '',
  xLabel: '',
  yLabel: '',
  // data:           PropTypes.array.isRequired, //consider: chart = { data: [{x:3, y:4, label: "thundercloud"},], xlabel: "", ylabel: "" }
  // xRange: d3.time.scale().range([margin,width-margin]).domain([new Date(2011, 08, 15), new Date(2015, 01, 01)]);,
  // yRange: d3.scale.linear().range([height-margin, margin]).domain([77, 100]);,
  // r: d3.scale.linear().domain([50,200]).range([0,20]);
};
