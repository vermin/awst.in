/** @jsx jsx */
import * as d3 from 'd3';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { jsx, css } from '@emotion/core';
import { TransitionGroup } from 'react-transition-group';

import * as defaults from '../utils/defaults';
import { XAxis, YAxis } from './BarAxis';
import Bar from './Bar';

// style charts
const styledRect = props => css`
g g.bar rect {
  fill: ${`${props.colors.primary}77`};
}
g g.bar text {
  text-anchor: middle;
  display: none;
}
g g.bar:hover 
  { 
    rect {
      fill: ${props.colors.primary};
    }
    text {
      display: block;
      fill: ${props.colors.text};
  }
`;

export class BarChart extends Component {
  node = React.createRef();

  // shouldComponentUpdate(nextProps) {
  //   if (this.props.data === nextProps.data) {
  //     return false;
  //   }
  //   return true;
  // }

  componentDidMount() {
    // this.createBarChart();
  }

  componentDidUpdate() {
    // this.createBarChart();
  }

  componentWillUnmount() {}

  x() {
    const { data, sizing } = this.props;
    const chartWidth = sizing.width - sizing.margin.left - sizing.margin.right;
    return d3
      .scaleBand()
      .domain(data.map(d => d.x))
      .range([this.props.sizing.margin.left, chartWidth]);
  }

  y() {
    const { data, sizing } = this.props;
    const chartHeight =
      sizing.height - sizing.margin.top - sizing.margin.bottom;

    return d3
      .scaleLinear()
      .domain(d3.extent(data, d => d.y))
      .nice()
      .range([chartHeight, sizing.margin.top]);
  }

  makeBar(bar) {
    // let percent = bar.y/this.props.data.length*100;
    //d => y(d3.min(yData)) - y(d.y));

    const xS = this.x();
    const yS = this.y();
    const x = xS(bar.x);
    const y = bar.y ? yS(bar.y) : yS(0);
    const height = yS(0) - yS(bar.y);
    const width = bar.width ? bar.width : xS(xS.bandwidth());
    const props = {
      label: bar.label,
      x,
      y,
      width,
      height,
      key: `bar-${x}-${y}-${bar.label}`,
      // key: `bar-${x}-${y}-${bar.label}-${Math.random(0, 1000)}`,
    };

    return <Bar {...props} />;
  }

  render() {
    return (
      <g className="barchart" css={styledRect(this.props)} ref={this.node}>
        <XAxis
          ticks={this.props.data.map(d => d.x)}
          scale={this.x()}
          label={this.props.xLabel}
          color={this.props.colors.axis}
          sizing={this.props.sizing}
        />
        <YAxis
          ticks={this.props.data.length}
          scale={this.y()}
          label={this.props.yLabel}
          color={this.props.colors.axis}
          sizing={this.props.sizing}
        />
        <g className="bars">{this.props.data.map(this.makeBar.bind(this))}</g>
      </g>
    );
  }
}

BarChart.propTypes = {
  data: PropTypes.array.isRequired, // { data: [{x:int, y:int, label: ""},{...}], xlabel: "", ylabel: "" }
  colors: PropTypes.object,
  sizing: PropTypes.object,
  titlePrefix: PropTypes.string,
  xLabel: PropTypes.string,
  yLabel: PropTypes.string,
};

BarChart.defaultProps = {
  colors: defaults.colors,
  sizing: {
    width: 640,
    height: 480,
    margin: {
      top: 20,
      right: 20,
      bottom: 50,
      left: 70,
    },
  },
  titlePrefix: '',
  xLabel: 'xAxis',
  yLabel: 'yAxis',
  // xRange: d3.time.scale().range([margin,width-margin]).domain([new Date(2011, 08, 15), new Date(2015, 01, 01)]);,
  // yRange: d3.scale.linear().range([height-margin, margin]).domain([77, 100]);,
  // r: d3.scale.linear().domain([50,200]).range([0,20]);
};

export default BarChart;
