/** @jsx jsx */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import { jsx, css } from '@emotion/core';

class XAxis extends Component {
  node = React.createRef();

  shouldComponentUpdate(nextProps) {
    if (this.props === nextProps) {
      return false;
    }
    return true;
  }

  componentDidUpdate() {
    this.renderAxis();
  }

  componentDidMount() {
    this.renderAxis();
  }

  renderAxis() {
    const axis = d3.axisBottom(this.props.scale).tickSizeOuter(0);
    d3.select(this.node.current).call(axis);
  }

  render() {
    const { sizing } = this.props;
    let translate = `translate(0, ${sizing.height -
      sizing.margin.top -
      sizing.margin.bottom})`;

    return (
      <g
        className="xAxis"
        transform={translate}
        ref={this.node}
        css={css`
          .tick text {
            transform: rotate(70deg) translate(6px, -7px);
            text-anchor: start;
          }
        `}
      >
        <AxisText
          color={this.props.color}
          text={this.props.label}
          x={sizing.width - sizing.margin.right}
        />
      </g>
    );
  }
}

class YAxis extends Component {
  constructor(props) {
    super();
    this.state = { xPos: 0, yPos: props.sizing.margin.top };
  }
  node = React.createRef();

  componentDidUpdate() {
    this.renderAxis();
  }

  componentDidMount() {
    this.renderAxis();
    // need to detect the position of the last tick
    // AFAIK the d3 function for axis generation is a blackbox
    // thus, we pierce the DOM abstraction to get correct positioning for our label
    // assign to state
    // and update component with new values
    const yPos = parseFloat(
      getComputedStyle(
        this.node.current.querySelector('.tick:last-of-type text'),
      ).fontSize,
    );
    const xPos = this.node.current
      .querySelector('.tick:last-of-type text')
      .getAttribute('x');
    this.setState({ yPos, xPos });
  }

  renderAxis() {
    const axis = d3.axisLeft(this.props.scale);
    d3.select(this.node.current).call(axis);
  }

  componentWillUnmount() {
    this.state = {
      yPos: 0,
      xPos: 0,
    };
  }

  render() {
    let translate = `translate(${this.props.sizing.margin.left},0)`;
    return (
      <g className="yAxis" transform={translate} ref={this.node}>
        <AxisText
          color={this.props.color}
          text={this.props.label}
          y={this.state.yPos}
          x={this.state.xPos}
        />
      </g>
    );
  }
}

const AxisText = function svgTextLabelGenerator(props) {
  return (
    <text
      fill={props.color}
      x={props.x}
      y={props.y}
      textAnchor="start"
      fontWeight="bold"
    >
      {props.text}
    </text>
  );
};

export { XAxis, YAxis };
