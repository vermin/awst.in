/** @jsx jsx */

import { jsx, css } from '@emotion/core';
import styled from '@emotion/styled';

import BarChart from './BarChart';
import HistogramChart from './HistogramChart';
import ScatterChart from './ScatterChart';

/* Design Goal
 *  <Sparkline
    className='visitors'
    sizing = { ... }
    data={
      data : [{value, label},{...},...]
      data.yAxisLabel : "Counties",
      data.xAxisLabel : "Unemployment (%)"
      [...],
      labels: { ... }
      }
    />,
 * */

// Think about extracting these into a common set of params or props
// const svg = d3.select(node)
// const x = ...
// const y = ...
// const xAxis = ...
// const yAxis = ...

const StyledSVG = styled.svg(props => ({
  background: props.background ? props.background : 'white',
  width: props.width ? props.width : '100%',
  height: props.height ? props.height : 480,
}));

// const SVG = ({ ...props }) => <StyledSVG />;

const SVG = css`
  background: white;
  width: 300;
  height: 400;
`;

export { HistogramChart, BarChart, ScatterChart, StyledSVG, SVG };
