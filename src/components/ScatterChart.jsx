import * as d3 from 'd3';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { jsx, css } from '@emotion/core';

export default class ScatterChart extends Component {
  componentDidMount() {
    this.createChart();
  }

  componentDidUpdate() {
    d3.select(this.node)
      .selectAll('g')
      .remove();
    this.createChart();
  }

  // Method to update the cirlces using D3
  createChart() {
    const { colors, data, sizing } = this.props;

    // style charts
    const styledSVG = css`
      background-color: ${colors.background};
      width: ${sizing.width};
    `;

    // set display bounds
    const chartWidth = sizing.width - sizing.margin.left - sizing.margin.right;
    const chartHeight =
      sizing.height - sizing.margin.top - sizing.margin.bottom;
    const x = d3
      .scaleLinear()
      .domain(d3.extent(data, d => d.x))
      .nice()
      .range([sizing.margin.left, chartWidth]);

    const svg = d3.select(this.node).attr('class', styledSVG);

    // Use the .enter() method to get your entering elements, and assign their positions
    svg
      .append('g')
      .selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
      .attr('r', d => d.r)
      .attr('fill', colors.primary)
      .attr('cx', d => x(d.x))
      .attr('cy', d => d.y)
      .append('title')
      .text(d => d.label);
  }

  render() {
    const { className, sizing } = this.props;
    return (
      <svg
        className={className}
        width={sizing.width}
        height={sizing.height}
        ref={node => {
          this.node = node;
        }}
      >
        {/* <g transform={`translate(${sizing.margin.left}, ${sizing.margin.top})`} /> */}
      </svg>
    );
  }
}
ScatterChart.propTypes = {
  className: PropTypes.string,
  colors: PropTypes.object,
  data: PropTypes.array,
  sizing: PropTypes.object,
  titlePrefix: PropTypes.string,
  // x: PropTypes.number,
  // y: PropTypes.number,
  xLabel: PropTypes.string,
  yLabel: PropTypes.string,
};

// Set default properties
ScatterChart.defaultProps = {
  className: 'chart',
  data: [
    { x: 1, y: 1, r: 4, label: 'meep' },
    { x: 100, y: 100, r: 14, label: 'meep' },
  ],
  // r: `d3.scale.linear().domain([50,200]).range([0,20])`,
  sizing: {
    width: 400,
    height: 270,
    margin: {
      top: 20,
      right: 20,
      bottom: 50,
      left: 70,
    },
  },
  colors: {
    axis: 'green',
    background: 'white',
    primary: 'turquoise',
    text: 'green',
  },
  titlePrefix: '',
  xLabel: '',
  yLabel: '',
  // data:           PropTypes.array.isRequired, //consider: chart = { data: [{x:3, y:4, label: "thundercloud"},], xlabel: "", ylabel: "" }
  // xRange: d3.time.scale().range([margin,width-margin]).domain([new Date(2011, 08, 15), new Date(2015, 01, 01)]);,
  // yRange: d3.scale.linear().range([height-margin, margin]).domain([77, 100]);,
};

// Render application
// ReactDOM.render(
//     <App width={400} height = {400}/>,
//     document.getElementById('main')
// );
